import Q from 'q'
import http from 'vue-resource/src/http'
import { options } from 'vue-resource/src/util'
import exception from './exception'
import config from '../../config'

function plugin (Vue) {
    if (plugin.installed) {
        return
    }

    let $config = process.env.NODE_ENV === 'production' ? config.build : config.dev

    Vue.api = function () {}

    Vue.api.fetch = function (params) {
        return Q.promise((resolve, reject) => {
            http
                .interceptors
                .push((req, next) => {
                    req.headers.set('X-From-MCloud', '1')
                    next()
                })

            http
                .post($config.api.url, params)
                .then((response) => {
                    console.log('Get response from api', params)

                    if (response.body.data !== undefined) {
                        console.log('Get correct response form api')

                        resolve(response.body)
                    } else {
                        console.warn('Parse api response failed')
                        reject({
                            id: exception.json_parse_failed
                        })
                    }
                })
        })
    }

    Object.defineProperties(Vue.prototype, {
        $api: {
            get () {
                return options(Vue.api, this, this.$options.api)
            }
        }
    })
}

export default plugin
