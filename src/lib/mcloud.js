import { options } from 'vue-resource/src/util'

function plugin (Vue) {
    if (plugin.installed) {
        return
    }

    Vue.isQin = function () {
        let result = false

        try {
            let ua = navigator.userAgent.toString()
            if (ua.indexOf('Qing') >= 0) {
                result = true
            }
        } catch (e) {
        }

        return result
    }

    Object.defineProperties(Vue.prototype, {
        $Qin: {
            get () {
                return options(Vue.isQin, this, this.$options.isQin)
            }
        }
    })
}

export default plugin
