import Q from 'q'
import cookie from 'cookie-browser'
import http from 'vue-resource/src/http'
import { options } from 'vue-resource/src/util'
import exception from './exception'
import config from '../../config'

function plugin (Vue) {
    if (plugin.installed) {
        return
    }

    let $config = process.env.NODE_ENV === 'production' ? config.build : config.dev

    Vue.PonovoSession = function () {}
    Vue.PonovoSession.openid = ''
    Vue.PonovoSession.sessionid = ''
    Vue.PonovoSession.uid = ''
    Vue.PonovoSession.ticket = ''

    Vue.PonovoSession.t = function (t) {
        if (t) {
            Vue.PonovoSession.ticket = t
            cookie.set('ticket', t)
        } else if (cookie.get('ticket')) {
            Vue.PonovoSession.ticket = cookie.get('ticket')
        }

        return Vue.PonovoSession
    }

    Vue.PonovoSession.oauth = function () {
        let self = Vue.PonovoSession

        window.location = $config.api.url + 'm=mcloud&act=oauth&sid=' + self.sessionid
    }

    Vue.PonovoSession.init = function (comp) {
        let self = Vue.PonovoSession
        console.log('[' + comp + ']Start session init')

        return Q.Promise((resolve, reject) => {
            let sessionid = cookie.get('sid')
            let ticket = self.ticket

            if (sessionid === undefined) {
                sessionid = ''
            }

            console.log('[' + comp + ']Start api request in ')
            http
                .interceptors
                .push((req, next) => {
                    req.headers.set('X-From-MCloud', '1')
                    next()
                })

            http
                .post($config.api.url, {
                    m: 'mcloud',
                    act: 'session',
                    sessionid: sessionid,
                    ticket: ticket
                })
                .then((response) => {
                    console.log('[' + comp + ']Got response from api ', response.body.data)

                    if (response.body !== undefined && response.body.data !== undefined) {
                        let data = response.body.data

                        if (data.sessionid !== undefined && data.sessionid !== '') {
                            self.sessionid = data.sessionid

                            if (data.openid === undefined || data.openid === '' || data.openid === null) {
                                reject({
                                    error: exception.mcloud_api_call_fail,
                                    sessionid: data.sessionid,
                                    openid: data.openid
                                })
                            } else {
                                if (data.ponovouid === undefined || data.ponovouid === 0 || data.ponovouid === '' || data.ponovouid === '0') {
                                    reject({
                                        error: exception.user_not_binded,
                                        sessionid: data.sessionid,
                                        openid: data.openid
                                    })
                                } else {
                                    resolve(data)
                                }
                            }

                            cookie.set('sid', data.sessionid)
                        }
                    } else {
                        console.error('[' + comp + ']Session init failed', exception.session_init_failed)

                        reject({
                            error: exception.session_init_failed
                        })
                    }
                })
        })
    }

    Object.defineProperties(Vue.prototype, {
        $PonovoSession: {
            get () {
                return options(Vue.PonovoSession, this, this.$options.PonovoSession)
            }
        }
    })
}

export default plugin
