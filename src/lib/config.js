import { options } from 'vue-resource/src/util'
import config from '../../config'

function plugin (Vue) {
    if (plugin.installed) {
        return
    }

    Vue.PonovoConfig = function () {}

    let $config = process.env.NODE_ENV === 'production' ? config.build : config.dev

    for (let i in $config) {
        Vue.PonovoConfig[i] = $config[i]
    }

    Object.defineProperties(Vue.prototype, {
        $config: {
            get () {
                return options(Vue.PonovoConfig, this, this.$options.PonovoConfig)
            }
        }
    })
}

export default plugin
