let exception = {
    unknow_error: 9999,
    json_parse_failed: 9998,
    session_init_failed: 8000,
    user_not_binded: 7000,
    mcloud_api_call_fail: 9997
}

export default exception
