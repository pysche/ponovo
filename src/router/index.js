import Vue from 'vue'
import Router from 'vue-router'
import {Loading, Popup, Toast, Tabbar, TabItem} from 'yun-ui'
import VueResource from 'vue-resource'
import PonovoConfig from '../lib/config'
import PonovoApi from '@/lib/api'
import PonovoSession from '@/lib/session'
import Qin from '@/lib/mcloud'
import Demo from '@/components/Demo'
import Index from '@/components/Index'
import Login from '@/components/Login'
import Customer from '@/components/Customer/Index'
import CustomerAdd from '@/components/Customer/Add'
import Expense from '@/components/Expense/Index'
import ExpenseAdd from '@/components/Expense/Add'
import ExpenseEdit from '@/components/Expense/Edit'
import ExpenseShow from '@/components/Expense/Show'
import Error404 from '@/components/Error/E404'
import 'yun-ui/dist/yun/Tabbar/index.css'
import 'yun-ui/dist/yun/TabItem/index.css'
import 'yun-ui/dist/yun/Loading/index.css'
import 'yun-ui/dist/yun/Popup/index.css'
import 'yun-ui/dist/yun/Toast/index.css'

Vue.use(Router)
Vue.use(VueResource)
Vue.use(Qin)
Vue.use(PonovoConfig)
Vue.use(PonovoApi)
Vue.use(PonovoSession)
Vue.use(Toast)
Vue.use(Popup)

Vue.component(Tabbar.name, Tabbar)
Vue.component(TabItem.name, TabItem)
Vue.component(Loading.name, Loading)
Vue.component(Popup.name, Popup)

export default new Router({
    mode: 'history',
    routes: [
        {path: '/', name: 'Index', component: Index},
        {path: '/Login', name: 'Login', component: Login},
        {path: '/Customer', name: 'Customer', component: Customer},
        {path: '/Customer/add', name: 'CustomerAdd', component: CustomerAdd},
        {path: '/Expense', name: 'Expense', component: Expense},
        {path: '/Expense/page/:page', name: 'Expense', component: Expense},
        {path: '/Expense/add', name: 'ExpenseAdd', component: ExpenseAdd},
        {path: '/Expense/search/:searchText/page/:page', name: 'Expense', component: Expense},
        {path: '/Expense/Edit/:id', name: 'ExpenseEdit', component: ExpenseEdit},
        {path: '/Expense/:id', name: 'ExpenseShow', component: ExpenseShow},
        {path: '/Demo', name: 'Demo', component: Demo},
        {path: '*', component: Error404}
    ]
})
