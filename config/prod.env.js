
module.exports = {
  NODE_ENV: '"production"',
  ponovo: {
      area: {
          BC: {
              name: '北京公司',
              provs: {
                  1000: '北京'
              }
          },
          BB: {
              name: '重庆公司',
              provs: {
                  1010: '重庆'
              }
          },
          AA: {
              name: '大客户部',
              provs: {
                  1020: '大客户部'
              }
          },
          CQ: {
              name: '重庆',
              provs: {
                  1010: '重庆'
              }
          },
          BJ: {
              name: '京津唐',
              provs: {
                  1000: '北京',
                  1030: '天津',
                  1040: '河北北'
              }
          },
          HJ: {
              name: '黑吉',
              provs: {
                  1050: '黑龙江',
                  1060: '吉林'
              }
          },
          LN: {
              name: '辽宁',
              provs: {
                  1070: '辽宁',
                  1080: '内蒙古东'
              }
          },
          HB: {
              name: '华北',
              provs: {
                  1090: '河南',
                  1100: '河北南',
                  1110: '山西'
              }
          },
          XB: {
              name: '西北',
              provs: {
                  1120: '陕西',
                  1130: '宁夏',
                  1140: '甘肃',
                  1150: '青海',
                  1160: '内蒙古西'
              }
          },
          HZ: {
              name: '华中',
              provs: {
                  1170: '湖北',
                  1180: '湖南',
                  1190: '江西'
              }
          },
          XN: {
              name: '西南',
              provs: {
                  1200: '四川',
                  1210: '云南',
                  1220: '西藏'
              }
          },
          QG: {
              name: '黔桂',
              provs: {
                  1230: '贵州',
                  1240: '广西'
              }
          },
          HN: {
              name: '华南',
              provs: {
                  1250: '广东',
                  1260: '海南'
              }
          },
          HD: {
              name: '华东技术中心',
              provs: {
                  1270: '南京'
              }
          },
          SW: {
              name: '苏皖',
              provs: {
                  1280: '江苏',
                  1290: '安徽'
              }
          },
          SD: {
              name: '山东',
              provs: {
                  1300: '山东'
              }
          },
          ZM: {
              name: '浙闽',
              provs: {
                  1310: '浙江',
                  1320: '福建'
              }
          },
          XJ: {
              name: '新疆',
              provs: {
                  1330: '新疆'
              }
          },
          SH: {
              name: '上海',
              provs: {
                  1340: '上海'
              }
          }
      },
      area_prov: {

      },
      customer: {
          role: {
              1: '决策者',
              2: '购买者',
              3: '使用者'
          },
          stars: {
              1: '☆',
              2: '☆☆',
              3: '☆☆☆',
              4: '☆☆☆☆',
              5: '☆☆☆☆☆'
          },
          provs: {
            '11': {
                name: '北京市',
                codes: [ '0010' ]
            },
            '12': {
                name: '天津市',
                codes: [ '0022' ]
            },
            '13': {
                name: '河北省',
                codes: [ '0310', '0311', '0312', '0313', '0314', '0315', '0316', '0317', '0318', '0319', '0335' ]
            },
            '14': {
                name: '山西省',
                codes: [ '0350', '0351', '0352', '0353', '0354', '0355', '0356', '0357', '0358', '0359' ]
            },
            '15': {
                name: '内蒙古自治区',
                codes: [ '0470', '0471', '0472', '0473', '0474', '0475', '0476', '0477', '0478', '0479', '0482', '0483' ]
            },
            '21': {
                name: '辽宁省',
                codes: [ '0024', '0410', '0411', '0412', '0413', '0414', '0415', '0416', '0417', '0418', '0419', '0421', '0427', '0429' ]
            },
            '22': {
                name: '吉林省',
                codes: [ '0431', '0432', '0433', '0434', '0435', '0436', '0437', '0438', '0439', '0440' ]
            },
            '23': {
                name: '黑龙江省',
                codes: [ '0450', '0451', '0452', '0453', '0454', '0455', '0456', '0457', '0458', '0459' ]
            },
            '31': {
                name: '上海市',
                codes: [ '0021' ]
            },
            '32': {
                name: '江苏省',
                codes: [ '0025', '0510', '0511', '0512', '0513', '0514', '0515', '0516', '0517', '0518', '0519', '0523' ]
            },
            '33': {
                name: '浙江省',
                codes: [ '0570', '0571', '0572', '0573', '0574', '0575', '0576', '0577', '0578', '0579', '0580' ]
            },
            '34': {
                name: '安徽省',
                codes: [ '0550', '0551', '0552', '0553', '0554', '0555', '0556', '0557', '0558', '0559', '0561', '0562', '0563', '0564', '0565', '0566' ]
            },
            '35': {
                name: '福建省',
                codes: [ '0591', '0592', '0593', '0594', '0595', '0596', '0597', '0598', '0599' ]
            },
            '36': {
                name: '江西省',
                codes: [ '0790', '0791', '0792', '0793', '0794', '0795', '0796', '0797', '0798', '0799', '0701' ]
            },
            '37': {
                name: '山东省',
                codes: [ '0530', '0531', '0532', '0533', '0534', '0535', '0536', '0537', '0538', '0539', '0631' ]
            },
            '41': {
                name: '河南省',
                codes: [ '0370', '0371', '0372', '0373', '0374', '0375', '0376', '0377', '0378', '0379', '0391', '0392', '0393', '0394', '0395', '0396', '0398' ]
            },
            '42': {
                name: '湖北省',
                codes: [ '0027', '0710', '0711', '0712', '0713', '0714', '0715', '0716', '0717', '0718', '0719', '0722', '0724', '0728' ]
            },
            '43': {
                name: '湖南省',
                codes: [ '0730', '0731', '0732', '0733', '0734', '0735', '0736', '0737', '0738', '0739', '0743', '0744', '0745', '0746' ]
            },
            '44': {
                name: '广东省',
                codes: [ '0020', '0751', '0752', '0753', '0754', '0755', '0756', '0757', '0758', '0759', '0760', '0761', '0762', '0763', '0765', '0766', '0768', '0769', '0660', '0661', '0662', '0663' ]
            },
            '45': {
                name: '广西壮族自治区',
                codes: [ '0770', '0771', '0772', '0773', '0774', '0775', '0776', '0777', '0778', '0779' ]
            },
            '46': {
                name: '海南省',
                codes: [ '0890', '0898', '0899' ]
            },
            '50': {
                name: '重庆市',
                codes: [ '0023' ]
            },
            '51': {
                name: '四川省',
                codes: [ '0028', '0810', '0811', '0812', '0813', '0814', '0816', '0817', '0818', '0819', '0825', '0826', '0827', '0830', '0831', '0832', '0833', '0834', '0835', '0836', '0837', '0838', '0839', '0840' ]
            },
            '52': {
                name: '贵州省',
                codes: [ '0851', '0852', '0853', '0854', '0855', '0856', '0857', '0858', '0859' ]
            },
            '53': {
                name: '云南省',
                codes: [ '0870', '0871', '0872', '0873', '0874', '0875', '0876', '0877', '0878', '0879', '0691', '0692', '0881', '0883', '0886', '0887', '0888' ]
            },
            '54': {
                name: '西藏自治区',
                codes: [ '0891', '0892', '0893' ]
            },
            '61': {
                name: '陕西省',
                codes: [ '0029', '0910', '0911', '0912', '0913', '0914', '0915', '0916', '0917', '0919' ]
            },
            '62': {
                name: '甘肃省',
                codes: [ '0930', '0931', '0932', '0933', '0934', '0935', '0936', '0937', '0938', '0941', '0943' ]
            },
            '63': {
                name: '青海省',
                codes: [ '0971', '0972', '0973', '0974', '0975', '0976', '0977' ]
            },
            '64': {
                name: '宁夏回族自治区',
                codes: [ '0951', '0952', '0953', '0954' ]
            },
            '65': {
                name: '新疆维吾尔自治区',
                codes: [ '0990', '0991', '0992', '0993', '0994', '0995', '0996', '0997', '0998', '0999', '0902', '0903', '0906', '0908', '0909' ]
            },
            '71': {
                name: '台湾省',
                codes: [ '9999' ]
            },
            '72': {
                name: '香港特别行政区',
                codes: [ '9999' ]
            },
            '73': {
                name: '澳门特别行政区',
                codes: [ '9999' ]
            }
          }
      }
  }
}
